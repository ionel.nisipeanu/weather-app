from django.shortcuts import render, redirect, reverse
from django.http import HttpResponse
from django.conf import settings
import os, json
from django.contrib.auth.decorators import login_required


def index(request):

    get_menifest= open(os.path.join(settings.BASE_DIR, 'static/frontend/build/asset-manifest.json'), 'r').read()
    get_menifest=json.loads(get_menifest)
    build_path = 'frontend/build/'
    css=[]
    js=[]
    for i in get_menifest['files'].values():
        if i.endswith('.css'):
            css.append(build_path+i)
        if i.endswith('.js'):
            js.append(build_path+i)

    context={
        'title': 'Dashboard',
        'section': 'userdashboard_view',
        'css': css,
        'js': js,
    }
    return render(request, 'index.html', context)

def handler404(request, exception=None):
        
    get_menifest= open(os.path.join(settings.BASE_DIR, 'static/frontend/build/asset-manifest.json'), 'r').read()
    get_menifest=json.loads(get_menifest)
    build_path = 'frontend/build/'
    css=[]
    js=[]
    for i in get_menifest['files'].values():
        if i.endswith('.css'):
            css.append(build_path+i)
        if i.endswith('.js'):
            js.append(build_path+i)

    context={
        'title': 'Dashboard',
        'section': 'userdashboard_view',
        'css': css,
        'js': js,
    }
    return render(request, 'index.html', context, status=200)