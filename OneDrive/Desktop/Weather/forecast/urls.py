from django.urls import path
from .views import *
from .apis import *
from .auths import *
from .cron import *


urlpatterns = [
    path('', index),
    path('api/get_forecast/', get_forecast),
] + [
    path('get/tokens/', get_tokens),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('api/get_user_data/', get_user_data),
] + [
    path('create/animations/', create_animatipon),
    path('api/get_animations/', get_animations),
    path('api/delete_animations/<int:pk>/', delete_animation),
    path('get/owm/key/', get_owm_key),
    path('set/owm/key/', set_owm_key)
] + [
    path('api/create_location/', create_location),
    path('api/get_locations/', get_locations),
    path('api/delete_location/<int:pk>/', delete_location)
] + [
    path('location/data/', location_data),
]