import React, { useState, useEffect } from "react";
import New from "./new";
import ViewBox from "./ViewBox";
import DeleteBox from "./deleteBox";

export default function Animations() {
    const [modal, setModal] = useState(false);
    const [deleting, setDeleting] = useState(null);
    const [animations, setAnimations] = useState(null);
    const [animViewBox, setAnimViewBox] = useState(null);

    useEffect(() => {
        const access = localStorage.getItem('access');
        const base_url = localStorage.getItem('base_url');
        fetch(`${base_url}/api/get_animations/`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${access}`
            }
        }).then(response => {
            if (response.ok) {
                return response.json();
            } else {
                return null;
            }
        }).then(data => {
            if (data) {
                setAnimations(data);
            }
        });
    }, [modal, deleting]);

    const Trows = animations?.map((item, index) => (
        <tr key={index} className="w-full p-2 border border-gray-300">
            <td className="text-left p-2">{item.name}</td>
            <td className="text-left p-2 flex flex-row items-center">
                <svg onClick={() => setAnimViewBox(item.file)} xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 cursor-pointer mx-3 shadow-lg" viewBox="0 0 576 512">
                    <path d="M288 32c-80.8 0-145.5 36.8-192.6 80.6C48.6 156 17.3 208 2.5 243.7c-3.3 7.9-3.3 16.7 0 24.6C17.3 304 48.6 356 95.4 399.4C142.5 443.2 207.2 480 288 480s145.5-36.8 192.6-80.6c46.8-43.5 78.1-95.4 93-131.1c3.3-7.9 3.3-16.7 0-24.6c-14.9-35.7-46.2-87.7-93-131.1C433.5 68.8 368.8 32 288 32zM144 256a144 144 0 1 1 288 0 144 144 0 1 1 -288 0zm144-64c0 35.3-28.7 64-64 64c-7.1 0-13.9-1.2-20.3-3.3c-5.5-1.8-11.9 1.6-11.7 7.4c.3 6.9 1.3 13.8 3.2 20.7c13.7 51.2 66.4 81.6 117.6 67.9s81.6-66.4 67.9-117.6c-11.1-41.5-47.8-69.4-88.6-71.1c-5.8-.2-9.2 6.1-7.4 11.7c2.1 6.4 3.3 13.2 3.3 20.3z" />
                </svg>

                <svg onClick={() => setDeleting(item)} xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 cursor-pointer mx-3 shadow-lg" viewBox="0 0 448 512">
                    <path d="M135.2 17.7C140.6 6.8 151.7 0 163.8 0H284.2c12.1 0 23.2 6.8 28.6 17.7L320 32h96c17.7 0 32 14.3 32 32s-14.3 32-32 32H32C14.3 96 0 81.7 0 64S14.3 32 32 32h96l7.2-14.3zM32 128H416V448c0 35.3-28.7 64-64 64H96c-35.3 0-64-28.7-64-64V128zm96 64c-8.8 0-16 7.2-16 16V432c0 8.8 7.2 16 16 16s16-7.2 16-16V208c0-8.8-7.2-16-16-16zm96 0c-8.8 0-16 7.2-16 16V432c0 8.8 7.2 16 16 16s16-7.2 16-16V208c0-8.8-7.2-16-16-16zm96 0c-8.8 0-16 7.2-16 16V432c0 8.8 7.2 16 16 16s16-7.2 16-16V208c0-8.8-7.2-16-16-16z" />
                </svg>
            </td>
        </tr>
    ));

    return (
        <div className="w-full h-full flex flex-col items-center bg-transparent">
            {modal && <New modal={modal} setModal={setModal} />}
            {animViewBox && <ViewBox modal={animViewBox} setModal={setAnimViewBox} />}
            {deleting && <DeleteBox modal={deleting} setModal={setDeleting} />}
            <div className="w-full md:w-10/12 lg:w-8/12 p-4 my-3 mt-12 bg-white rounded-md shadow-lg">
                <div className="w-full p-2 flex justify-between items-center">
                    <h1 className="text-2xl font-bold">Animations</h1>
                    <button className="p-2 px-4 text-white bg-blue-600 hover:bg-blue-700 transition-all rounded-md shadow-lg" onClick={() => setModal(true)}>Create</button>
                </div>

                <hr className="w-full border border-gray-300"></hr>

                <div className="w-full overflow-x-auto">
                    <table className="w-full" border={1}>
                        <thead className="w-full bg-gray-100 p-2">
                            <tr className="w-full">
                                <th className="text-left p-2">Name</th>
                                <th className="text-left p-2">Actions</th>
                            </tr>
                        </thead>
                        <tbody className="w-full">
                            {Trows}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}
