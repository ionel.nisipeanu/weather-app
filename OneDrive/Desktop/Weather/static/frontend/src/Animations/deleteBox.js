import React from "react";

export default function DeleteBox({ modal, setModal }) {
    function deleteAnim() {
        const access = localStorage.getItem('access');
        const base_url = localStorage.getItem('base_url');
        fetch(`${base_url}/api/delete_animations/${modal.id}/`, {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${access}`
            }
        }).then(response => {
            if (response.ok) {
                setModal(null);
            }
        }).catch(error => {
            console.error("Eroare la ștergerea animației:", error);
        });
    }

    return (
        <div className={(modal ? "flex" : "hidden") + " fixed left-0 top-0 z-50 w-screen h-screen items-center justify-center backdrop-contrast-50 transition-all"}>
            <div className="w-11/12 md:w-7/12 xl:w-5/12 max-h-5/6 flex flex-col shadow-lg relative bg-white rounded-lg anim-pop">
                <button className="absolute top-1 right-1 p-2 text-gray-600 rounded-full shadow-lg bg-slate-200 hover:bg-slate-300" onClick={() => setModal(null)}>
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-5 h-5">
                        <path strokeLinecap="round" strokeLinejoin="round" d="M6 18L18 6M6 6l12 12" />
                    </svg>
                </button>
                <h1 className="text-md text-gray-800 p-2 font-semibold text-center mb-2">Do you want to delete the animation {modal.name}?</h1>

                <div className="w-full p-2 flex justify-center items-center">
                    <button className="p-2 px-4 text-white bg-red-600 hover:bg-red-700 transition-all rounded-md shadow-lg" onClick={() => setModal(null)}>Cancel</button>
                    <button className="p-2 px-4 text-white bg-blue-600 hover:bg-blue-700 transition-all rounded-md shadow-lg ml-2" onClick={deleteAnim}>Delete</button>
                </div>
            </div>
        </div>
    );
}
