import React from "react";

export default function ViewBox({ modal, setModal }) {
    return (
        <div className={(modal ? "flex" : "hidden") + " fixed left-0 top-0 z-50 w-screen h-screen items-center justify-center backdrop-contrast-50 transition-all"}>
            <div className="w-11/12 md:w-5/12 xl:w-3/12 max-h-5/6 flex flex-col shadow-lg relative bg-white rounded-lg anim-pop">
                <button className="absolute top-1 right-1 p-2 text-gray-600 rounded-full shadow-lg bg-slate-200 hover:bg-slate-300" onClick={() => setModal(false)}>
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-5 h-5">
                        <path strokeLinecap="round" strokeLinejoin="round" d="M6 18L18 6M6 6l12 12" />
                    </svg>
                </button>
                <h1 className="text-md text-gray-800 p-2 font-semibold text-center">View Animation</h1>
                <hr className="border-gray-400 dark:border-gray-700 my-1" />
                
                {modal && (
                    <video className="w-full rounded-lg object-cover" src={modal} autoPlay loop></video>
                )}
            </div>
        </div>
    );
}
