import React, { useState } from "react";
import { Loader } from "../skeleton";

export default function New({ modal, setModal }) {
    const [requesting, setRequesting] = useState(false);
    const [error, setError] = useState(null);
    const [name, setName] = useState('');

    const handleSubmit = (event) => {
        event.preventDefault();

        setRequesting(true);
        const access = localStorage.getItem('access');
        const base_url = localStorage.getItem('base_url');

        if (!name) {
            setError('Please fill all the fields');
            setRequesting(false);
            return;
        }

        let formData = new FormData();
        formData.append('name', name);
        fetch(`${base_url}/api/create_location/`, {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${access}`
            },
            body: formData
        })
        .then(response => response.json())
        .then(data => {
            if (data.success) {
                setModal(false);
            } else {
                setError(data.error);
            }
            setRequesting(false);
        })
        .catch(() => {
            setError('An error occurred while creating the location');
            setRequesting(false);
        });
    };

    return (
        <div className={`${modal ? "flex" : "hidden"} fixed left-0 top-0 z-50 w-screen h-screen items-center justify-center backdrop-contrast-50 transition-all`}>
            <div className="w-11/12 md:w-3/4 xl:w-7/12 max-h-5/6 flex flex-col shadow-lg relative bg-white rounded-lg anim-pop">
                <button className="absolute top-1 right-1 p-2 text-gray-600 rounded-full shadow-lg bg-slate-200 hover:bg-slate-300" onClick={() => setModal(false)}>
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-5 h-5">
                        <path strokeLinecap="round" strokeLinejoin="round" d="M6 18L18 6M6 6l12 12" />
                    </svg>
                </button>
                <h1 className="text-md text-gray-800 p-2 font-semibold text-center">New Location</h1>
                <hr className="border-gray-400 dark:border-gray-700 my-1"></hr>

                <div className="w-full p-2 min-h-[200px]">
                    <form onSubmit={handleSubmit} className="flex flex-col w-full p-3 items-center relative">
                        <div className="flex flex-col my-2 w-full">
                            <label htmlFor="name" className="m-1 text-gray-700 text-xs">Name:</label>
                            <input 
                                type="text" 
                                id="name" 
                                name="name" 
                                className="border border-gray-300 p-2 px-4 w-full text-xs focus:border-blue-500 outline-none rounded-full" 
                                onChange={(e) => setName(e.target.value)} 
                            />
                        </div>

                        {requesting && (
                            <div className="w-full absolute bottom-0 left-0 h-full backdrop-blur text-green-500">
                                <Loader />
                            </div>
                        )}

                        {error && <p className="text-red-500 text-sm text-center p-2">{error}</p>}

                        <button type="submit" className="w-full md:w-1/2 bg-blue-600 hover:bg-blue-700 rounded-full text-white px-8 py-2 my-2 text-sm focus:outline-none focus:ring focus:ring-blue-300">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    );
}
