import React, { useState, useEffect, useRef } from 'react';
import { NavLink } from 'react-router-dom';
import { useAppContext } from './appContexts';
import { Skeleton } from './skeleton';
import CurrentWeather from './fronts/currentWeather';
import TodaysForecast from './fronts/todaysForecast';
import NextThreeDaysForecast from './fronts/nextThreeDaysForecast';

const App = () => {
    const [place, setPlace] = useState('');
    const base_url = localStorage.getItem('base_url');
    const videoRef = useRef(null);
    const tabRef = useRef(null);
    const [currentWeather, setCurrentWeather] = useState(null);
    const [forecast, setForecast] = useState(null);
    const [threeDaysForecast, setThreeDaysForecast] = useState(null);
    const [searchInput, setSearchInput] = useState('');
    const [currentTime, setCurrentTime] = useState(new Date().toLocaleTimeString());

    const { App, User } = useAppContext();

    // Get local IP and set the place to the detected city
    useEffect(() => {
        const getLocalIp = async () => {
            try {
                const response = await fetch(`http://ip-api.com/json/`);
                const data = await response.json();
                setPlace(data.city);
            } catch (error) {
                console.error("Error fetching IP data: ", error);
            }
        };

        getLocalIp();
    }, []);

    // Fetch weather data for the specified place
    useEffect(() => {
        if (!place) return;

        const fetchWeatherData = async () => {
            try {
                const response = await fetch(`${base_url}/api/get_forecast/?place=${place}`);
                const data = await response.json();
                setCurrentWeather(data.weather);
                setForecast(data.forecast);
                setThreeDaysForecast(data.next_three_days_forecast);
            } catch (error) {
                console.error("Error fetching weather data: ", error);
            }
        };

        fetchWeatherData();
    }, [place, base_url]);

    // Play background video when available
    useEffect(() => {
        if (videoRef.current) {
            videoRef.current.play();
        }
    });

    return (
        <div className='w-full min-h-screen flex flex-col items-center bg-sky-100 select-none'>
            <video
                ref={videoRef}
                className='w-full h-screen object-cover bg-sky-900 z-0 fixed top-0 left-0'
                autoPlay
                loop
                muted
                src={`${base_url}${currentWeather ? currentWeather.bg : 'bgs.mp4'}`}
                type="video/mp4"
            ></video>

            <div className='w-full flex flex-col justify-center z-10'>
                <h1 className='text-6xl font-bold text-white text-center my-4'>Weather Forecast</h1>
                <header ref={tabRef} className='w-full h-16 bg-sky-300 flex justify-between items-center bg-sky-800 shadow-2xl'>
                    <span></span>
                    <div className='w-80 flex relative'>
                        <input
                            className='w-full pl-4 pr-4 py-2 text-white bg-sky-300/25 rounded-l-lg focus:outline-none focus:shadow-outline'
                            type="text"
                            placeholder="Search Location"
                            name="search"
                            value={searchInput}
                            onChange={(e) => setSearchInput(e.target.value)}
                        ></input>
                        <button
                            className='w-24 pl-4 pr-4 py-2 text-white bg-sky-300/25 rounded-r-lg focus:outline-none focus:shadow-outline'
                            type="submit"
                            onClick={() => setPlace(searchInput)}
                        >Search</button>
                    </div>
                    <div className='flex items-center justify-end mx-2'>
                        {User ?
                            <NavLink to='/dashboard' className='px-4 py-2 text-white bg-sky-300/25 rounded-lg focus:outline-none focus:shadow-outline'>Dashboard</NavLink> :
                            <NavLink to='/login' className='px-4 py-2 text-white bg-sky-300/25 rounded-lg focus:outline-none focus:shadow-outline'>Login</NavLink>}
                    </div>
                </header>
            </div>

            <div className='w-full min-h-screen flex justify-center z-10'>
                <main className='w-full lg:w-9/12 xl:w-7/12 h-full bg-sky-300/25 flex justify-center shadow-2xl'>
                    <div className='w-full p-2'>
                        <p className='w-full text-white text-left text-2xl font-bold'>
                            {currentWeather?.address} <span className='text-regular text-sm'>as of {currentTime}</span>
                        </p>

                        {currentWeather ? <CurrentWeather data={currentWeather} /> : <Skeleton rows={3} />}
                        {forecast ? <TodaysForecast data={forecast} /> : <Skeleton rows={3} />}
                        {threeDaysForecast ? <NextThreeDaysForecast data={threeDaysForecast} /> : <Skeleton rows={3} />}
                    </div>
                </main>
            </div>
        </div>
    );
};

export default App;
