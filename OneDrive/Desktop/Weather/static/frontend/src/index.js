import React from 'react';
import ReactDOM from 'react-dom/client';
import { Route, Routes, BrowserRouter as Router } from 'react-router-dom';
import './index.css';
import App from './App';
import Dashboard from './dashboard/dashboard';
import Login from './auths/login';
import { MyContextProvider } from './appContexts';

// Set base URL depending on the environment
if (process.env.NODE_ENV === 'development') {
  localStorage.setItem('base_url', 'http://127.0.0.1:8000');
} else {
  localStorage.setItem('base_url', window.location.origin);
}

const root = ReactDOM.createRoot(document.getElementById('root'));

// Render the application with context provider and routing
root.render(
  <React.StrictMode>
    <Router>
      <MyContextProvider>
        <Routes>
          <Route path="/*" element={<App />} />
          <Route path="/dashboard/*" element={<Dashboard />} />
          <Route path="/login" element={<Login />} />
        </Routes>
      </MyContextProvider>
    </Router>
  </React.StrictMode>
);
