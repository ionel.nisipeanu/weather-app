import React, { useEffect, useRef, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useAppContext } from '../appContexts';
import Sidenave from "./utils/sidenave";
import { Route, Routes } from "react-router-dom";

import Dash from "./dash";
import Animations from "../Animations/animations";
import Api from "../api/api";
import Locations from "../Locations/locations";

export default function Dashboard() {
    const { User, App } = useAppContext();
    const navigate = useNavigate();
    const [showSidenave, setShowSidenave] = useState(false);
    const sidenavRef = useRef(null);

    const toggleSidenave = () => {
        setShowSidenave(!showSidenave);
    };

    useEffect(() => {
        function handleClickOutside(event) {
            if (sidenavRef.current && !sidenavRef.current.contains(event.target)) {
                setShowSidenave(false);
            }
        }
        document.addEventListener("mousedown", handleClickOutside);
        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, []);

    useEffect(() => {
        if (!User) {
            navigate('/');
        }
    }, [User, navigate]);

    return (
        <div className="flex flex-row h-screen w-full">
            {/* Side nav */}
            <div
                ref={sidenavRef}
                className={`z-20 w-60 overflow-y-auto bg-white absolute h-screen lg:static top-0 lg:block flex-shrink-0 transition-transform duration-200 ease-in-out border-box hide_scroll_bar ${showSidenave ? 'translate-x-0' : '-translate-x-full'}`}
            >
                <button
                    className="z-20 absolute top-4 right-2 rounded-md lg:hidden focus:outline-none focus:shadow-outline-purple text-green-600"
                    onClick={toggleSidenave}
                >
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                        <path strokeLinecap="round" strokeLinejoin="round" d="M6 18 18 6M6 6l12 12" />
                    </svg>
                </button>

                <Sidenave chng={toggleSidenave} />
            </div>

            {/* Main content */}
            <div className="w-full h-screen flex flex-col">
                {/* Header */}
                <div className="z-10 bg-white w-full lg:w-auto mx-0 text-gray-800 border-b border-gray-300 px-4 py-1 flex h-14 justify-between items-center">
                    <div>
                        <button
                            className="z-20 absolute top-4 left-2 rounded-md focus:outline-none focus:shadow-outline-purple text-green-600"
                            onClick={toggleSidenave}
                        >
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                                <path strokeLinecap="round" strokeLinejoin="round" d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5" />
                            </svg>
                        </button>
                    </div>
                    <div></div>
                </div>

                <div className="w-full h-full flex flex-col bg-gray-100 items-center overflow-y-auto px-1">
                    <Routes>
                        <Route path="/*" element={<Dash />} />
                        <Route path="/animations" element={<Animations />} />
                        <Route path="/api" element={<Api />} />
                        <Route path="/locations" element={<Locations />} />
                    </Routes>
                </div>
            </div>
        </div>
    );
}
