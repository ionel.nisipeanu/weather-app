import React from "react";
import LocationData from "./LocationData";

export default function Dash() {
    return (
        <div className="w-full flex flex-col items-center">
            <div className="w-full md:w-11/12 xl:w-9/12">
                <h1 className="text-xl my-4">Welcome to Dashboard!</h1>
                <LocationData />
            </div>
        </div>
    );
}
