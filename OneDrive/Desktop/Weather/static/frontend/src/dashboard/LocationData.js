import React, { useState } from "react";

export default function LocationData() {
    const [location, setLocation] = useState('');
    const [locData, setLocData] = useState(null);
    const [error, setError] = useState(null);

    const searchLocation = () => {
        const access = localStorage.getItem('access');
        const base_url = localStorage.getItem('base_url');

        setLocData(null);
        setError(null);

        if (!location) {
            setError('Please fill all the fields');
            return;
        }

        fetch(`${base_url}/location/data/?location_name=${location}`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${access}`
            }
        })
        .then(response => response.json())
        .then(data => {
            if (data.error) {
                setError(data.error);
            } else {
                setLocData(data);
            }
        })
        .catch(() => {
            setError('An error occurred while fetching data');
        });
    };

    return (
        <div className="w-full flex flex-col items-center p-2 bg-white shadow-lg rounded-lg my-4">
            <h1 className="w-full text-center text-lg font-bold">Search Location</h1>
            <div className="flex flex-col w-full p-3 items-center">
                <div className="flex flex-col my-2 w-full">
                    <label htmlFor="location" className="m-1 text-gray-700 text-xs">Location Name:</label>
                    <input 
                        type="text" 
                        id="location"
                        placeholder="Search location" 
                        value={location} 
                        className="border border-gray-300 p-2 px-4 w-full text-xs focus:border-blue-500 outline-none rounded-full" 
                        onChange={(e) => setLocation(e.target.value)} 
                    />
                </div>
                {error && <p className="text-red-500 text-xs">{error}</p>}
                <button 
                    type="button" 
                    onClick={searchLocation} 
                    className="w-full md:w-1/2 bg-blue-600 hover:bg-blue-700 rounded-full text-white px-8 py-2 my-2 text-sm focus:outline-none focus:ring focus:ring-blue-300"
                >
                    Search
                </button>
            </div>

            {locData && (
                <div className="w-full flex flex-col items-center p-2 my-4">
                    <h1 className="text-lg my-2 text-center">Location: {locData.address}</h1>
                    <a 
                        href={locData.csv} 
                        target="_blank" 
                        rel="noopener noreferrer" 
                        className="text-blue-500 hover:text-blue-600 text-sm text-center my-1 p-2 px-4 border border-blue-500 hover:border-blue-600 rounded-full" 
                        download={`${locData.address}.csv`}
                    >
                        Data in CSV File.
                    </a>
                </div>
            )}
        </div>
    );
}
