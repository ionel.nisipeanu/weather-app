import { createContext, useContext, useState, useEffect } from 'react';

const AppContext = createContext();

export const MyContextProvider = ({ children }) => {
  const [App, setApp] = useState(null);
  const [User, setUser] = useState(null);

  useEffect(() => {
    const getUserData = async () => {
      const refresh = localStorage.getItem('refresh');
      const base_url = localStorage.getItem('base_url');

      // Refresh the access token if a refresh token is available
      if (refresh) {
        try {
          const response = await fetch(`${base_url}/api/token/refresh/`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({ refresh: refresh }),
          });
          
          if (response.ok) {
            const data = await response.json();
            localStorage.setItem('access', data.access);
          } else {
            console.error('Failed to refresh token');
          }
        } catch (error) {
          console.error('Error refreshing token:', error);
        }
      }

      // Fetch user data with the updated access token
      try {
        const response = await fetch(`${base_url}/api/get_user_data/`, {
          method: 'GET',
          headers: {
            'Authorization': `Bearer ${localStorage.getItem('access')}`,
          },
        });
        
        if (response.ok) {
          const data = await response.json();
          setUser(data);
        } else {
          setUser(null);
        }
      } catch (error) {
        console.error('Error fetching user data:', error);
        setUser(null);
      }
    };

    getUserData();
    
    // Set an interval to refresh the user data every 2 seconds
    const interval = setInterval(() => {
      getUserData();
    }, 2000);

    return () => clearInterval(interval);
  }, []);

  return (
    <AppContext.Provider value={{ User, setUser, App }}>
      {children}
    </AppContext.Provider>
  );
};

export const useAppContext = () => {
  return useContext(AppContext);
};
