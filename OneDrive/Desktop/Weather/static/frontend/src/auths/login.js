import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAppContext } from '../appContexts';

export default function Login() {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');
    const { App, User } = useAppContext();
    const navigate = useNavigate();

    useEffect(() => {
        if (User) {
            navigate('/dashboard');
        }
    }, [User, navigate]);

    const handleSubmit = async (e) => {
        e.preventDefault();
        const base_url = localStorage.getItem('base_url');
        const form = new FormData();
        form.append('username', username);
        form.append('password', password);

        try {
            const response = await fetch(`${base_url}/get/tokens/`, {
                method: 'POST',
                body: form,
            });
            const data = await response.json();
            if (data.success) {
                localStorage.setItem('access', data.access);
                localStorage.setItem('refresh', data.refresh);
                navigate('/dashboard');
            } else {
                setError(data.error);
            }
        } catch (error) {
            setError('An error occurred. Please try again.');
        }
    };

    return (
        <div className="flex flex-col items-center justify-center h-screen w-full bg-sky-400">
            <div className="text-center mt-24">
                <h2 className="text-xl text-white my-3 tracking-tight font-extrabold sm:text-3xl md:text-4xl">
                    Sign in to your account
                </h2>
            </div>
            <div className="flex justify-center my-2 mx-4 md:mx-0">
                <form className="w-full max-w-xl bg-sky-200/75 rounded-lg shadow-lg p-6 text-black" onSubmit={handleSubmit}>
                    <div className="flex flex-wrap -mx-3 mb-6">
                        <div className="w-full px-3 mb-6">
                            <label className="block uppercase tracking-wide text-xs font-bold mb-2" htmlFor="username">Username</label>
                            <input
                                id="username"
                                type="text"
                                value={username}
                                onChange={(e) => setUsername(e.target.value)}
                                className="w-full bg-transparent autofill:bg-transparent font-medium border-2 border-sky-400 shadow-inner rounded-lg py-3 px-3 leading-tight focus:outline-none"
                                placeholder="Username"
                                required
                            />
                        </div>
                        <div className="w-full px-3 mb-6">
                            <label className="block uppercase tracking-wide text-xs font-bold mb-2" htmlFor="password">Password</label>
                            <input
                                id="password"
                                type="password"
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                                className="w-full bg-transparent autofill:bg-transparent font-medium border-2 border-sky-400 shadow-inner rounded-lg py-3 px-3 leading-tight focus:outline-none"
                                placeholder="Password"
                                required
                            />
                        </div>
                        <div className="w-full px-3 mb-3">
                            {error && <p className="my-1 text-red-400 text-sm text-center">{error}</p>}
                            <button className="w-full shadow-lg py-3 bg-blue-600 hover:bg-blue-700 focus:shadow-outline focus:outline-none rounded-lg text-white" type="submit">
                                Sign in
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    );
}
