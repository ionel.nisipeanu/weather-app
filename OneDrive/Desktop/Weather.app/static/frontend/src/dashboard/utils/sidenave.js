import React from "react";
import { NavLink } from 'react-router-dom';
import { useAppContext } from "../../appContexts";

const Sidenave = (props) => {
    const { App, User } = useAppContext();

    // Funcția de deconectare
    function logOut() {
        localStorage.clear();
        window.location.href = '/';
    }

    return (
        <div className='flex flex-col justify-between w-full h-full'>
            <div className="w-full h-full flex flex-col">
                <NavLink to='/dashboard' className="w-full flex flex-row">
                    <h1 className="text-xl text-gray-800 text-center my-3 pl-3">{App ? App.name : "Weather App"}</h1>
                </NavLink>

                <div className="w-full flex flex-col pt-6">
                    <NavLink 
                        to="/dashboard" 
                        className={({ isActive }) => (isActive ? 'active-class' : '') + " flex items-center w-full py-2 pl-10 pr-3 text-sm text-gray-900 hover:text-black transition duration-300 ease-in-out cursor-pointer box-border"} 
                        onClick={props.chng}
                    >
                        <svg xmlns="http://www.w3.org/2000/svg" className="w-4 h-4" viewBox="0 0 512 512">
                            <path d="M.2 468.9C2.7 493.1 23.1 512 48 512l96 0 320 0c26.5 0 48-21.5 48-48l0-96c0-26.5-21.5-48-48-48l-48 0 0 80c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-80-64 0 0 80c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-80-64 0 0 80c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-80-80 0c-8.8 0-16-7.2-16-16s7.2-16 16-16l80 0 0-64-80 0c-8.8 0-16-7.2-16-16s7.2-16 16-16l80 0 0-64-80 0c-8.8 0-16-7.2-16-16s7.2-16 16-16l80 0 0-48c0-26.5-21.5-48-48-48L48 0C21.5 0 0 21.5 0 48L0 368l0 96c0 1.7 .1 3.3 .2 4.9z"/>
                        </svg>
                        <span className="ml-4">Dashboard</span>
                    </NavLink>
                    <NavLink 
                        to="/dashboard/locations" 
                        className={({ isActive }) => (isActive ? 'active-class' : '') + " flex items-center w-full py-2 pl-10 pr-3 text-sm text-gray-900 hover:text-black transition duration-300 ease-in-out cursor-pointer box-border"} 
                        onClick={props.chng}
                    >
                        <svg xmlns="http://www.w3.org/2000/svg" className="w-4 h-4" viewBox="0 0 384 512">
                            <path d="M215.7 499.2C267 435 384 279.4 384 192C384 86 298 0 192 0S0 86 0 192c0 87.4 117 243 168.3 307.2c12.3 15.3 35.1 15.3 47.4 0zM192 128a64 64 0 1 1 0 128 64 64 0 1 1 0-128z"/>
                        </svg>
                        <span className="ml-4">Locations</span>
                    </NavLink>
                    <NavLink 
                        to="/dashboard/animations" 
                        className={({ isActive }) => (isActive ? 'active-class' : '') + " flex items-center w-full py-2 pl-10 pr-3 text-sm text-gray-900 hover:text-black transition duration-300 ease-in-out cursor-pointer box-border"} 
                        onClick={props.chng}
                    >
                        <svg xmlns="http://www.w3.org/2000/svg" className="w-4 h-4" viewBox="0 0 512 512">
                            <path d="M278.5 215.6L23 471c-9.4 9.4-9.4 24.6 0 33.9s24.6 9.4 33.9 0l57-57h68c49.7 0 97.9-14.4 139-41c11.1-7.2 5.5-23-7.8-23c-5.1 0-9.2-4.1-9.2-9.2c0-4.1 2.7-7.6 6.5-8.8l81-24.3c2.5-.8 4.8-2.1 6.7-4l22.4-22.4c10.1-10.1 2.9-27.3-11.3-27.3l-32.2 0c-5.1 0-9.2-4.1-9.2-9.2c0-4.1 2.7-7.6 6.5-8.8l112-33.6c4-1.2 7.4-3.9 9.3-7.7C506.4 207.6 512 184.1 512 160c0-41-16.3-80.3-45.3-109.3l-5.5-5.5C432.3 16.3 393 0 352 0s-80.3 16.3-109.3 45.3L139 149C91 197 64 262.1 64 330v55.3L253.6 195.8c6.2-6.2 16.4-6.2 22.6 0c5.4 5.4 6.1 13.6 2.2 19.8z"/>
                        </svg>
                        <span className="ml-4">Animations</span>
                    </NavLink>
                    <NavLink 
                        to="/dashboard/api" 
                        className={({ isActive }) => (isActive ? 'active-class' : '') + " flex items-center w-full py-2 pl-10 pr-3 text-sm text-gray-900 hover:text-black transition duration-300 ease-in-out cursor-pointer box-border"} 
                        onClick={props.chng}
                    >
                        <svg xmlns="http://www.w3.org/2000/svg" className="w-4 h-4" viewBox="0 0 512 512">
                            <path d="M418.4 157.9c35.3-8.3 61.6-40 61.6-77.9c0-44.2-35.8-80-80-80c-43.4 0-78.7 34.5-80 77.5L136.2 151.1C121.7 136.8 101.9 128 80 128c-44.2 0-80 35.8-80 80s35.8 80 80 80c12.2 0 23.8-2.7 34.1-7.6L259.7 407.8c-2.4 7.6-3.7 15.8-3.7 24.2c0 44.2 35.8 80 80 80s80-35.8 80-80c0-27.7-14-52.1-35.4-66.4l37.8-207.7zM156.3 232.2c2.2-6.9 3.5-14.2 3.7-21.7l183.8-73.5c3.6 3.5 7.4 6.7 11.6 9.5L317.6 354.1c-5.5 1.3-10.8 3.1-15.8 5.5L156.3 232.2z"/>
                        </svg>
                        <span className="ml-4">OWM API</span>
                    </NavLink>
                </div>
            </div>
            <div className="w-full p-3">
                <button className="w-full py-2 px-4 bg-gray-300 cursor-pointer rounded-lg text-center" onClick={logOut}>Log Out</button>
            </div>
        </div>
    );
};

export default Sidenave;
