import React, { useState, useEffect } from "react";
import New from "./new";
import DeleteBox from "./deleteBox";

export default function Locations() {
    const [modal, setModal] = useState(false);
    const [deleting, setDeleting] = useState(null);
    const [locations, setLocations] = useState(null);

    // Fetch locations whenever modal or deleting state changes
    useEffect(() => {
        const fetchLocations = async () => {
            const access = localStorage.getItem('access');
            const base_url = localStorage.getItem('base_url');
            const response = await fetch(`${base_url}/api/get_locations/`, {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${access}`
                }
            });

            if (response.ok) {
                const data = await response.json();
                setLocations(data);
            } else {
                console.error('Failed to fetch locations');
            }
        };

        fetchLocations();
    }, [modal, deleting]);

    // Map location data to table rows
    const Trows = locations?.map((item, index) => (
        <tr key={index} className="w-full p-2 border border-gray-300">
            <td className="text-left p-2">{item.name}</td>
            <td className="text-left p-2 flex flex-row items-center">
                <svg onClick={() => setDeleting(item)} xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 cursor-pointer mx-3 shadow-lg" viewBox="0 0 448 512">
                    <path d="M135.2 17.7C140.6 6.8 151.7 0 163.8 0H284.2c12.1 0 23.2 6.8 28.6 17.7L320 32h96c17.7 0 32 14.3 32 32s-14.3 32-32 32H32C14.3 96 0 81.7 0 64S14.3 32 32 32h96l7.2-14.3zM32 128H416V448c0 35.3-28.7 64-64 64H96c-35.3 0-64-28.7-64-64V128zm96 64c-8.8 0-16 7.2-16 16V432c0 8.8 7.2 16 16 16s16-7.2 16-16V208c0-8.8-7.2-16-16-16zm96 0c-8.8 0-16 7.2-16 16V432c0 8.8 7.2 16 16 16s16-7.2 16-16V208c0-8.8-7.2-16-16-16zm96 0c-8.8 0-16 7.2-16 16V432c0 8.8 7.2 16 16 16s16-7.2 16-16V208c0-8.8-7.2-16-16-16z"/>
                </svg>
            </td>
        </tr>
    ));

    return (
        <div className="w-full h-full flex flex-col items-center bg-transparent">
            {modal && <New modal={modal} setModal={setModal} />}
            {deleting && <DeleteBox modal={deleting} setModal={setDeleting} />}
            <div className="w-full md:w-10/12 lg:w-8/12 p-4 my-3 mt-12 bg-white rounded-md shadow-lg">
                <div className="w-full p-2 flex justify-between items-center">
                    <h1 className="text-2xl font-bold">Locations</h1>
                    <button className="p-2 px-4 text-white bg-blue-600 hover:bg-blue-700 transition-all rounded-md shadow-lg" onClick={() => setModal(true)}>New</button>
                </div>

                <hr className="w-full border border-gray-300"></hr>

                <div className="w-full overflow-x-auto">
                    <table className="w-full" border={1}>
                        <thead className="w-full">
                            <tr className="w-full bg-gray-100 p-2">
                                <th className="text-left p-2">Name</th>
                                <th className="text-left p-2">Actions</th>
                            </tr>
                        </thead>
                        <tbody className="w-full">
                            {Trows}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}
