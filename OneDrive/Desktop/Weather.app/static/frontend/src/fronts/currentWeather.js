import React from "react";
import Chart from 'react-apexcharts';


export default function CurrentWeather(props) {
    const data = props.data

    const options = {
        chart: {
        height: 350,
        type: 'radialBar',
        },
        series: [data?.humidity],
        labels: ['Humidity'],
    }
    
    return (
        <div className='mt-4 p-4 flex flex-col items-left'>
            <p className='text-white text-6xl font-sans font-semibold'>{data?.temperature.celsius}°</p>
            <div className='w-full pt-1 flex flex-row items-center'>
                <p className='text-white text-3xl font-sans font-semibold capitalize'>{data?.detailed_status}</p>
                <img src={data?.weather_icon_url} className='h-16'></img>
            </div>
            <div className='w-full flex flex-row items-center p-2'>
                <img src='https://upload.wikimedia.org/wikipedia/commons/9/99/Rain_icon.svg' className='h-6'></img>
                <p className='text-white text-3xl font-sans font-semibold capitalize ml-2'>{data?.precipitation}</p>
                <img src='data:image/svg+xml;utf8;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIGRhdGEtbmFtZT0iTGF5ZXIgMSIgdmlld0JveD0iMCAwIDUxMiA1MTIiPjxnIGRhdGEtbmFtZT0iJmx0O0dyb3VwJmd0OyI+PHBhdGggZmlsbD0iIzAwYWNlYSIgZD0iTTM3LDE4N0gyNjMuOTIzYTYxLjcsNjEuNywwLDAsMC0uMTcyLTEyMy40LDEwLDEwLDAsMSwwLDAsMjAsNDEuNyw0MS43LDAsMCwxLC4xNzIsODMuNEgzN2ExMCwxMCwwLDAsMCwwLDIwWiIgZGF0YS1uYW1lPSImbHQ7UGF0aCZndDsiLz48cGF0aCBmaWxsPSIjMDBhY2VhIiBkPSJNMzcgMjMzSDM3Ni45NDFhNjEuNDI4IDYxLjQyOCAwIDEgMC0uMTcxLTEyMi44NTYgMTAgMTAgMCAwIDAgMCAyMEE0MS40MjggNDEuNDI4IDAgMSAxIDM3Ni45NDEgMjEzSDM3YTEwIDEwIDAgMCAwIDAgMjB6TTQyMy42NjYgMjY5SDM3YTEwIDEwIDAgMCAwIDAgMjBINDIzLjY2NmE0MS40MzIgNDEuNDMyIDAgMSAxIC4wMTIgODIuODY0IDEwIDEwIDAgMCAwIDAgMjBBNjEuNDMyIDYxLjQzMiAwIDEgMCA0MjMuNjY2IDI2OXoiIGRhdGEtbmFtZT0iJmx0O1BhdGgmZ3Q7Ii8+PHBhdGggZmlsbD0iIzAwYWNlYSIgZD0iTTIyNS44ODUsMzI2SDM3YTEwLDEwLDAsMCwwLDAsMjBIMjI1Ljg4NWE0MS4zMSw0MS4zMSwwLDEsMSwuMDExLDgyLjYxOCwxMCwxMCwwLDAsMCwwLDIwQTYxLjMxLDYxLjMxLDAsMSwwLDIyNS44ODUsMzI2WiIgZGF0YS1uYW1lPSImbHQ7UGF0aCZndDsiLz48L2c+PC9zdmc+' className='h-10 ml-2'></img>
                <p className='text-white text-3xl font-sans font-semibold capitalize ml-2'>{data?.wind.speed} KM/h</p>
            </div>
            <div className='w-full mt-4 p-4 pt-1 flex flex-col items-left bg-white shadow-2xl rounded-lg'>
                <Chart options={options} series={options.series} type="radialBar" height={300} />
            </div>
        </div>
    );
}