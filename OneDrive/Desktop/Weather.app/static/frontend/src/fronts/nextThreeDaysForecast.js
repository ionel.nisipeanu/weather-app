import React from "react";

export default function NextThreeDaysForecast({ data }) {
    const forecastElements = data?.map((item, index) => (
        <div key={index} className="w-full my-1 flex flex-row items-center justify-between shadow-lg p-1 rounded-lg">
            <p className="text-sm font-sans capitalize">{item.time}</p>
            <div className="flex flex-row items-center">
                <p className="text-xs font-sans capitalize">{item.detailed_status}</p>
                {item.weather_icon && <img src={item.weather_icon} alt="Weather icon" className="h-6" />}
            </div>
            <div className="flex flex-row items-center">
                <img src="https://upload.wikimedia.org/wikipedia/commons/9/99/Rain_icon.svg" alt="Rain icon" className="h-4" />
                <p className="text-lg font-sans capitalize ml-1">{item.rain}</p>
            </div>
            <p className="text-md font-sans capitalize pl-3">{item.temp}°</p>
        </div>
    ));

    return (
        <div className="w-full mt-4 p-2 pt-1 flex flex-col items-left bg-white shadow-2xl rounded-lg">
            <p className="text-black text-xl font-semibold font-sans p-1 text-blue-700">Next 3 Days Forecast</p>
            {forecastElements}
        </div>
    );
}
