import React from 'react';

function Skeleton({ rows = 1 }) {
    const elements = [];

    for (let i = 0; i < rows; i++) {
        elements.push(<div key={i} className="h-3 bg-gray-200 rounded-full dark:bg-gray-700 w-full my-2.5"></div>);
    }

    return (
        <div role="status" className="w-full p-2 animate-pulse">
            {elements}
        </div>
    );
}

function Loader() {
    return (
        <div role="status" className="w-full h-full flex items-center justify-center bg-transparent">
            <div className="flex items-center justify-center p-4 bg-white rounded">
                <div className="w-5 h-5 bg-transparent animate-spin rounded-full border-2 border-solid border-blue-500 border-r-transparent"></div>
                <span className="text-sm text-gray-600 ml-2 animate-pulse">Loading...</span>
            </div>
        </div>
    );
}

export { Skeleton, Loader };
