import React, { useState, useEffect } from "react";
import { Loader } from "../skeleton";

export default function Api() {
    const [requesting, setRequesting] = useState(false);
    const [error, setError] = useState(null);
    const [apiKey, setApiKey] = useState('');

    useEffect(() => {
        const fetchApiKey = async () => {
            const access = localStorage.getItem('access');
            const base_url = localStorage.getItem('base_url');

            try {
                const response = await fetch(`${base_url}/get/owm/key/`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${access}`
                    }
                });

                if (response.ok) {
                    const data = await response.json();
                    setApiKey(data.key);
                }
            } catch (error) {
                console.error('Failed to fetch API key:', error);
            }
        };

        fetchApiKey();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        setRequesting(true);
        const access = localStorage.getItem('access');
        const base_url = localStorage.getItem('base_url');

        const formData = new FormData();
        formData.append('key', apiKey);

        try {
            const response = await fetch(`${base_url}/set/owm/key/`, {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${access}`
                },
                body: formData
            });

            const data = await response.json();

            if (data.success) {
                setError('');
            } else {
                setError(data.error);
            }
        } catch (error) {
            setError('An error occurred. Please try again.');
        } finally {
            setRequesting(false);
        }
    };

    return (
        <div className="w-full flex flex-col items-center">
            <div className="w-full md:w-11/12 xl:w-9/12 p-4 bg-white rounded shadow mt-12">
                <h1 className="text-xl my-4">Open Weather API</h1>
                <form onSubmit={handleSubmit} className="flex flex-col w-full p-3 items-center relative">
                    <div className="flex flex-col my-2 w-full">
                        <label htmlFor="apiKey" className="m-1 text-gray-700 text-xs">API Key:</label>
                        <input
                            type="text"
                            id="apiKey"
                            name="apiKey"
                            className="border border-gray-300 p-2 px-4 w-full text-xs focus:border-blue-500 outline-none rounded-full"
                            value={apiKey}
                            onChange={(e) => setApiKey(e.target.value)}
                        />
                        <p className="text-xs text-gray-500">
                            Don't have an API key? Get one <a target="_blank" rel="noopener noreferrer" href="https://home.openweathermap.org/" className="text-blue-500">here</a>
                        </p>
                    </div>

                    {requesting && (
                        <div className="w-full absolute bottom-0 left-0 h-full backdrop-blur text-green-500">
                            <Loader />
                        </div>
                    )}

                    {error && <p className="text-red-500 text-sm text-center p-2">{error}</p>}

                    <button type="submit" className="w-full md:w-1/2 bg-blue-600 hover:bg-blue-700 rounded-full text-white px-8 py-2 my-2 text-sm focus:outline-none focus:ring focus:ring-blue-300">
                        Save
                    </button>
                </form>
            </div>
        </div>
    );
}
