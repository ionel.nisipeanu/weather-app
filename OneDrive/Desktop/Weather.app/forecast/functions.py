import spacy
from .models import Animations

def make_decition(weather=""):
    """
    Alege animația cea mai relevantă pe baza descrierii vremii.
    """
    # Încărcarea modelului spaCy
    try:
        nlp = spacy.load("en_core_web_md")
    except OSError:
        spacy.cli.download("en_core_web_md")
        nlp = spacy.load("en_core_web_md")

    # Obținerea animațiilor din baza de date
    anims = Animations.objects.all()
    anim_list = [anim.name for anim in anims]

    if not anim_list:
        return '/'

    try:
        # Procesarea textului de vreme și a animațiilor cu spaCy
        processed_weather = nlp(weather)
        processed_anims = [nlp(anim_name) for anim_name in anim_list]

        # Calcularea scorurilor de similaritate
        similarity_scores = [processed_weather.similarity(anim) for anim in processed_anims]

        # Găsirea animației relevante
        most_relevant_index = similarity_scores.index(max(similarity_scores))
        most_relevant_animation_name = anim_list[most_relevant_index]

        # Obținerea fișierului animației relevante
        animation = Animations.objects.filter(name=most_relevant_animation_name).first()

        if animation:
            return animation.file.url
        else:
            return '/'
    except Exception as e:
        print(f"Eroare: {e}")
        return '/'
