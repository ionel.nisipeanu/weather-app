from django.shortcuts import render, redirect, reverse
from django.contrib.auth import authenticate, login, logout
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from rest_framework import generics, permissions, status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework_simplejwt.views import TokenRefreshView as SimpleJWTTokenRefreshView
from rest_framework.decorators import api_view, permission_classes
from .models import *

@api_view(['POST'])
@permission_classes([AllowAny])
def get_tokens(request):
    """
    Autentifică utilizatorul și returnează token-urile JWT dacă autentificarea are succes.
    """
    username = request.data.get("username")
    password = request.data.get("password")

    user = authenticate(request, username=username, password=password)
    if user:
        login(request, user)
        refresh = RefreshToken.for_user(user)
        return Response({
            'success': 'Autentificare reușită',
            'refresh': str(refresh),
            'access': str(refresh.access_token),
        }, status=status.HTTP_200_OK)
    else:
        return Response({"error": "Credențiale greșite"}, status=status.HTTP_400_BAD_REQUEST)

class TokenRefreshView(SimpleJWTTokenRefreshView):
    """
    O clasă pentru reîmprospătarea token-ului JWT.
    """
    pass

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_user_data(request):
    """
    Returnează datele utilizatorului autentificat.
    """
    user = request.user
    data = {
        'username': user.username,
    }
    return Response(data, status=status.HTTP_200_OK)
