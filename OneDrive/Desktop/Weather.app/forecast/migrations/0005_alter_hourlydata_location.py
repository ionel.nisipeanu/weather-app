# Generated by Django 5.0.3 on 2024-04-08 22:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('forecast', '0004_hourlydata'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hourlydata',
            name='location',
            field=models.TextField(),
        ),
    ]
