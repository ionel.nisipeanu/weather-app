from django.db import models

class Animations(models.Model):
    """
    Model pentru animații.
    """
    name = models.TextField()
    file = models.FileField(upload_to='animations')

    def __str__(self):
        return self.name

    def delete(self, *args, **kwargs):
        """
        Suprascrierea metodei delete pentru a șterge fișierul asociat de pe disc.
        """
        storage, path = self.file.storage, self.file.path
        super().delete(*args, **kwargs)
        storage.delete(path)


class OWMapi(models.Model):
    """
    Model pentru stocarea cheii API OpenWeatherMap.
    """
    key = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.key


class Location(models.Model):
    """
    Model pentru locații.
    """
    name = models.TextField()
    act_name = models.TextField(null=True, blank=True)
    act_country = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name


class HourlyData(models.Model):
    """
    Model pentru datele meteo orare.
    """
    location = models.TextField()
    date = models.TextField()
    hour = models.TextField()

    temperature = models.FloatField()
    humidity = models.FloatField()
    wind_speed = models.FloatField()
    wind_deg = models.FloatField()
    status = models.TextField()
    weather_icon_url = models.URLField()
    rain = models.FloatField()

    def __str__(self):
        return f"{self.location} {self.date} {self.hour}"
