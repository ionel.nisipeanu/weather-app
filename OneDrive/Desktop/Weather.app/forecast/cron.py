from .models import OWMapi, Location, HourlyData
from apscheduler.schedulers.background import BackgroundScheduler
import pyowm
from datetime import datetime
import time

def get_hourly_forecast():
    """
    Preia prognoza meteo orară pentru locațiile stocate și salvează datele în baza de date.
    """
    try:
        owm1 = OWMapi.objects.get(pk=1)
    except OWMapi.DoesNotExist:
        owm1 = OWMapi.objects.create(id=1)

    owm = pyowm.OWM(owm1.key)
    all_locations = Location.objects.all()

    for location in all_locations:
        try:
            observation = owm.weather_manager().weather_at_place(location.name)
            weather = observation.weather
            now = datetime.now()
            date = now.strftime("%Y-%m-%d")
            hour = now.strftime("%H")
            time.sleep(1)  # Evită apelurile rapide consecutive la API

            HourlyData.objects.create(
                location=location.name,
                date=date,
                hour=hour,
                temperature=weather.temperature('celsius')['temp'],
                humidity=weather.humidity,
                wind_speed=weather.wind()['speed'] * 3.6,  # Convertire din m/s în km/h
                wind_deg=weather.wind()['deg'],
                status=weather.detailed_status,
                weather_icon_url=weather.weather_icon_url(),
                rain=weather.rain.get('1h', 0.0),
            )
        except Exception as e:
            print(f"Eroare pentru locația {location.name}: {e}")

# Configurarea și pornirea scheduler-ului
scheduler = BackgroundScheduler()
scheduler.add_job(get_hourly_forecast, trigger='cron', hour='*', max_instances=1)
scheduler.start()
