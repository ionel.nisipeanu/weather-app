
import json, random, string, pytz, requests
from django.shortcuts import render, redirect, reverse
from django.http import HttpResponse, JsonResponse
from django.contrib.auth.decorators import login_required
from datetime import datetime, timedelta
from django.views.decorators.csrf import csrf_exempt
from django.db.models import F, Q
import pyowm
# from timezonefinder import TimezoneFinder
import pytz, pandas, tempfile, base64
from .functions import make_decition
from .models import *


from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework_simplejwt.views import TokenRefreshView
from rest_framework.decorators import api_view, permission_classes

@api_view(['GET'])
@permission_classes([AllowAny])
def get_forecast(request):
    try:
        owm1=OWMapi.objects.get(pk=1)
    except:
        owm1=OWMapi.objects.create(key="", id=1).save()
        owm1=OWMapi.objects.get(pk=1)

    try:
        owm = pyowm.OWM(owm1.key)
        place=request.GET.get('place')
        observation = owm.weather_manager().weather_at_place(place)
        w = observation.weather
        location = observation.location

        # tf = TimezoneFinder()
        # timezone_name = tf.timezone_at(lng=location.lon, lat=location.lat)

        # local_time = datetime.now(pytz.timezone(timezone_name))

        # time_string = f"{local_time.hour}:{local_time.minute} {timezone_name}"

        weather_data = {
            'address': f"{location.name}, {location.country}",
            'temperature': {
                'celsius': w.temperature('celsius')['temp'],
                'fahrenheit': w.temperature('fahrenheit')['temp']
            },
            'humidity': w.humidity,
            'detailed_status': w.detailed_status,
            'weather_icon_url': w.weather_icon_url(),
            'wind': {
                'speed': format(w.wind()['speed'] * 3.6, '.1f'), 
                'direction': w.wind()['deg'] 
            },
            'precipitation': w.rain.get('1h', 0.0),
            'bg': make_decition(w.detailed_status),
        }
        

        forecast = owm.weather_manager().forecast_at_place(location.name, '3h').forecast.to_dict().get('weathers')
        forecast_data = []
        for forc in forecast[:9]:
            time=forc.get('reference_time')
            if len(forecast_data) == 8:
                time="23:59"
            else:
                time=datetime.fromtimestamp(time).strftime('%H:%M')
            temp=format(float(forc.get('temperature').get('temp') - 273.15), '.1f')
            detailed_status=forc.get('detailed_status')
            humidity=forc.get('humidity')
            weather_icon=f"https://openweathermap.org/img/wn/{forc.get('weather_icon_name')}.png"
            dict={
                'time': time,
                'temp': temp,
                'detailed_status': detailed_status,
                'humidity': humidity,
                'rain': forc.get('rain').get('3h', 0.0),
                'weather_icon': weather_icon
            }

            forecast_data.append(dict)

        # sort forecast data by time
        forecast_data = sorted(forecast_data, key=lambda k: k['time'])

        next_three_days_forecast = []
        for forc in forecast[8:32]:
            time=forc.get('reference_time')
            
            temp=format(float(forc.get('temperature').get('temp') - 273.15), '.1f')
            detailed_status=forc.get('detailed_status')
            humidity=forc.get('humidity')
            weather_icon=f"https://openweathermap.org/img/wn/{forc.get('weather_icon_name')}.png"
            dict={
                'time': time,
                'temp': temp,
                'detailed_status': detailed_status,
                'humidity': humidity,
                'rain': forc.get('rain').get('3h', 0.0),
                'weather_icon': weather_icon
            }

            next_three_days_forecast.append(dict)

        next_three_days_forecast = sorted(next_three_days_forecast, key=lambda k: k['time'])

        for i in next_three_days_forecast:
            i['time'] = datetime.fromtimestamp(i['time']).strftime('%a, %b -%d %H:%M')


        return Response({'weather': weather_data, 'forecast': forecast_data, 'next_three_days_forecast': next_three_days_forecast}, status=status.HTTP_200_OK)
    except Exception as e:
        print(e)
        return Response({'error': str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)




@api_view(['POST'])
@permission_classes([IsAuthenticated])
def create_animatipon(request):
    user=request.user
    name=request.data.get('name')
    file=request.FILES.get('file')

    if name and file:
        Animations.objects.create(name=name, file=file).save()
        return Response({'success': 'Animation created successfully'}, status=status.HTTP_200_OK)
    else:
        return Response({'error': 'Bad Request'}, status=status.HTTP_400_BAD_REQUEST)

    return Response({'error': 'Bad Request'}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_animations(request):
    Anim=Animations.objects.all()
    data=[]
    for i in Anim:
        data.append({
            'id': i.id,
            'name': i.name,
            'file': i.file.url
        })

    return Response(data, status=status.HTTP_200_OK)

@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
def delete_animation(request, pk):
    Anim=Animations.objects.get(id=pk)
    Anim.delete()
    return Response({'success': 'Animation deleted successfully'}, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_owm_key(request):
    try:
        owm=OWMapi.objects.get(pk=1)
    except:
        owm=OWMapi.objects.create(id=1).save()
        owm=OWMapi.objects.get(pk=1)

    return Response({'key': owm.key}, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def set_owm_key(request):
    try:
        owm=OWMapi.objects.get(pk=1)
    except:
        owm=OWMapi.objects.create(id=1).save()
        owm=OWMapi.objects.get(pk=1)

    try:
        key=request.data.get('key')

        owm.key=key
        owm.save()

        return Response({'success': 'Key updated successfully'}, status=status.HTTP_200_OK)
    except Exception as e:
        print(e)
        return Response({'error': 'Bad Request'}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def create_location(request):
    user=request.user
    name=request.data.get('name')
    
    if name:
        try:
            owm=OWMapi.objects.get(pk=1)
        except:
            owm=OWMapi.objects.create(id=1).save()
            owm=OWMapi.objects.get(pk=1)

        try:
            owm_obj = pyowm.OWM(owm.key)
            observation = owm_obj.weather_manager().weather_at_place(name)
            location = observation.location

            if Location.objects.filter(act_name=location.name, act_country=location.country).exists():
                return Response({'error': 'Location already exists'}, status=status.HTTP_400_BAD_REQUEST)

            Location.objects.create(name=name, act_name=location.name, act_country=location.country).save()

            return Response({'success': 'Location created successfully'}, status=status.HTTP_200_OK)
        
        except Exception as e:
            print(e)
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)

    else:
        return Response({'error': 'Bad Request'}, status=status.HTTP_400_BAD_REQUEST)

    return Response({'error': 'Bad Request'}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_locations(request):
    Anim=Location.objects.all()
    data=[]
    for i in Anim:
        data.append({
            'id': i.id,
            'name': i.name,
        })

    return Response(data, status=status.HTTP_200_OK)

@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
def delete_location(request, pk):
    Anim=Location.objects.get(id=pk)
    Anim.delete()
    return Response({'success': 'Animation deleted successfully'}, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def location_data(request):
    try:
        location_name=request.GET.get('location_name')
        location=Location.objects.filter(name__icontains=location_name).first()
        if not location:
            return Response({'error': 'Location not found'}, status=status.HTTP_404_NOT_FOUND)

        hist_data=HourlyData.objects.filter(location__icontains=location_name).order_by('-date', '-hour').values_list('date', 'hour', 'temperature', 'humidity', 'wind_speed', 'wind_deg', 'status', 'weather_icon_url', 'rain')

        #convert data to csv using pandas
        pd=pandas
        df = pd.DataFrame(hist_data)
        df.columns = ['date', 'hour', 'temperature', 'humidity', 'wind_speed', 'wind_deg', 'status', 'weather_icon_url', 'rain']

        woek_dir=tempfile.TemporaryDirectory()

        temp_csv_file = f"{woek_dir.name}/data.csv"
        df.to_csv(temp_csv_file, index=False)

        # convert csv to base64 for download in html
        with open(temp_csv_file, 'rb') as f:
            base64_encoded_data = base64.b64encode(f.read()).decode('utf-8')
        csv_base64_html = f"data:text/csv;base64,{base64_encoded_data}"
        woek_dir.cleanup()
        return Response({'address': f"{location.act_name}, {location.act_country}", 'csv': csv_base64_html}, status=status.HTTP_200_OK)

    except Exception as e:
        print(e)
        return Response({'error': str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
